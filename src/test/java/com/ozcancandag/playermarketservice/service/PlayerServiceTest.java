package com.ozcancandag.playermarketservice.service;

import com.ozcancandag.playermarketservice.model.PlayerResponse;
import com.ozcancandag.playermarketservice.repository.PlayerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class PlayerServiceTest {

    @Autowired
    private PlayerService playerService;

    @MockBean
    private PlayerRepository playerRepository;

    @Test
    public void queryShouldReturn404() {
        when(playerRepository.findById(1L)).thenReturn(Optional.empty());

        PlayerResponse playerResponse = playerService.queryPlayer(1L);

        assertEquals(404, playerResponse.getCode());
    }
}
