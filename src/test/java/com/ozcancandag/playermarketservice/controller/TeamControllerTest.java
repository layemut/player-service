package com.ozcancandag.playermarketservice.controller;

import com.ozcancandag.playermarketservice.model.ServiceResponse;
import com.ozcancandag.playermarketservice.model.TeamResponse;
import com.ozcancandag.playermarketservice.model.dto.TeamDto;
import com.ozcancandag.playermarketservice.service.TeamService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TeamControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TeamService teamService;

    @Test
    void queryTeamShouldReturn() throws Exception {
        TeamResponse teamResponse = new TeamResponse();
        teamResponse.setCode(200);
        teamResponse.setMessage(ServiceResponse.SUCCESS);
        teamResponse.setTeam(getMockDto());

        when(teamService.queryTeam(1L)).thenReturn(teamResponse);

        this.mockMvc.perform(get("/api/v1/team/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.team.name", is(teamResponse.getTeam().getName())))
                .andExpect(jsonPath("$.team.commissionRate", is(teamResponse.getTeam().getCommissionRate())));
    }

    private TeamDto getMockDto() {
        TeamDto teamDto = new TeamDto();
        teamDto.setId(1L);
        teamDto.setName("Beşiktaş");
        teamDto.setCommissionRate(5.5);
        teamDto.setPlayers(Collections.emptyList());
        return teamDto;
    }
}
