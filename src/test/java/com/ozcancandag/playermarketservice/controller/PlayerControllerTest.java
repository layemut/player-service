package com.ozcancandag.playermarketservice.controller;

import com.ozcancandag.playermarketservice.model.PlayerResponse;
import com.ozcancandag.playermarketservice.model.ServiceResponse;
import com.ozcancandag.playermarketservice.model.dto.PlayerDto;
import com.ozcancandag.playermarketservice.service.PlayerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.is;

@SpringBootTest
@AutoConfigureMockMvc
class PlayerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlayerService playerService;

    @Test
    void queryPlayerShouldReturn() throws Exception {
        PlayerResponse playerResponse = new PlayerResponse();
        playerResponse.setCode(200);
        playerResponse.setMessage(ServiceResponse.SUCCESS);
        playerResponse.setPlayer(getMockDto());

        when(playerService.queryPlayer(1L)).thenReturn(playerResponse);

        this.mockMvc.perform(get("/api/v1/player/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.player.name", is(playerResponse.getPlayer().getName())))
                .andExpect(jsonPath("$.player.surname", is(playerResponse.getPlayer().getSurname())));
    }

    @Test
    void deletePlayerShouldReturn() throws Exception {
        PlayerResponse playerResponse = new PlayerResponse();
        playerResponse.setCode(200);
        playerResponse.setMessage(ServiceResponse.SUCCESS);

        when(playerService.deletePlayer(1L)).thenReturn(playerResponse);

        this.mockMvc.perform(delete("/api/v1/player/1"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private PlayerDto getMockDto() {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setId(1L);
        playerDto.setAlias("test");
        playerDto.setName("TestName");
        playerDto.setSurname("TestSurname");
        playerDto.setMiddleName("TestMiddleName");
        playerDto.setTeamId(2L);
        playerDto.setMonthsOfExperience(22);
        playerDto.setAge(26);
        return playerDto;
    }
}
