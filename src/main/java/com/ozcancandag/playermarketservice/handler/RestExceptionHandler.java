package com.ozcancandag.playermarketservice.handler;

import com.ozcancandag.playermarketservice.model.ServiceResponse;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({EmptyResultDataAccessException.class, NoSuchElementException.class})
    protected ResponseEntity<ServiceResponse> handleEntityNotFound() {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setCode(HttpStatus.NOT_FOUND.value());
        serviceResponse.setMessage("Not found");
        return ResponseEntity.status(404).body(serviceResponse);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<ServiceResponse> handleIntegrity() {
        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.setCode(HttpStatus.BAD_REQUEST.value());
        serviceResponse.setMessage("Bad Request");
        return ResponseEntity.status(400).body(serviceResponse);
    }
}
