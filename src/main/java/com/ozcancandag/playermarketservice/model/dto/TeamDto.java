package com.ozcancandag.playermarketservice.model.dto;

import com.ozcancandag.playermarketservice.entity.Team;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeamDto {

    private Long id;
    private String name;
    private double commissionRate;
    private List<PlayerDto> players;

    public TeamDto(Team team) {
        this.id = team.getId();
        this.name = team.getName();
        this.commissionRate = team.getCommissionRate();
        if (team.getPlayers() != null) {
            this.players = team.getPlayers().stream().map(PlayerDto::new).collect(Collectors.toList());
        }
    }
}
