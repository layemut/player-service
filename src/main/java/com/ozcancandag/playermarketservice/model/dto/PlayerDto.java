package com.ozcancandag.playermarketservice.model.dto;

import com.ozcancandag.playermarketservice.entity.Player;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDto {
    private Long id;
    private String name;
    private String surname;
    private String middleName;
    private String alias;
    private int age;
    private int monthsOfExperience;
    private BigDecimal transferFee;
    private Long teamId;

    public PlayerDto(Player player) {
        this.id = player.getId();
        this.name = player.getName();
        this.surname = player.getSurname();
        this.middleName = player.getMiddleName();
        this.alias = player.getAlias();
        this.teamId = player.getTeam().getId();
        this.age = player.getAge();
        this.monthsOfExperience = player.getMonthsOfExperience();
        this.transferFee = BigDecimal.valueOf(player.getTransferFee()).setScale(2, RoundingMode.UP);
    }
}
