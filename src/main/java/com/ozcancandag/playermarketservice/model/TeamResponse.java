package com.ozcancandag.playermarketservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ozcancandag.playermarketservice.model.dto.TeamDto;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TeamResponse extends ServiceResponse {
    private TeamDto team;
}
