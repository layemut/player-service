package com.ozcancandag.playermarketservice.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.ozcancandag.playermarketservice.model.dto.PlayerDto;
import lombok.*;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PlayerResponse extends ServiceResponse {
    private PlayerDto player;
    private List<PlayerDto> players;
}
