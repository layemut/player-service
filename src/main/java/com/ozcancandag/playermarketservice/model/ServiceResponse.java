package com.ozcancandag.playermarketservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceResponse {
    public static final String SUCCESS = "success";
    protected int code;
    protected String message;
}
