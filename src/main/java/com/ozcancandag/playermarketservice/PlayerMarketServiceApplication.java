package com.ozcancandag.playermarketservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class PlayerMarketServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayerMarketServiceApplication.class, args);
	}

}
