package com.ozcancandag.playermarketservice.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.ozcancandag.playermarketservice.model.dto.PlayerDto;
import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Player extends AuditModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private String middleName;
    @Column(unique = true)
    private String alias;
    private int age;
    private int monthsOfExperience;
    private double transferFee;

    @JsonBackReference
    @ManyToOne(cascade = CascadeType.ALL)
    private Team team;

    public Player(PlayerDto playerDto) {
        this.id = playerDto.getId();
        this.name = playerDto.getName();
        this.surname = playerDto.getSurname();
        this.middleName = playerDto.getMiddleName();
        this.alias = playerDto.getAlias();
        this.age = playerDto.getAge();
        this.monthsOfExperience = playerDto.getMonthsOfExperience();
    }

    @PrePersist
    public void beforePersist() {
        this.transferFee = ((double) monthsOfExperience * 100000) / age;
    }
}
