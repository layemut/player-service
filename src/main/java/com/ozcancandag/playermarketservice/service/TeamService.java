package com.ozcancandag.playermarketservice.service;

import com.ozcancandag.playermarketservice.entity.Team;
import com.ozcancandag.playermarketservice.model.*;
import com.ozcancandag.playermarketservice.model.dto.TeamDto;
import com.ozcancandag.playermarketservice.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class TeamService {

    private final TeamRepository teamRepository;

    public TeamResponse queryTeam(Long teamId) {
        TeamResponse teamResponse = new TeamResponse();
        teamRepository.findById(teamId).ifPresentOrElse(team -> {
            teamResponse.setTeam(new TeamDto(team));
            teamResponse.setCode(HttpStatus.OK.value());
            teamResponse.setMessage(ServiceResponse.SUCCESS);
        }, () -> {
            teamResponse.setCode(HttpStatus.NOT_FOUND.value());
            teamResponse.setMessage("Team not found.");
        });
        return teamResponse;
    }

    public TeamResponse upsertTeam(TeamDto teamDto) {
        Team team = new Team(teamDto.getId(), teamDto.getName(), teamDto.getCommissionRate(), null);
        TeamResponse teamResponse = new TeamResponse();
        teamResponse.setTeam(new TeamDto(teamRepository.save(team)));
        teamResponse.setCode(HttpStatus.OK.value());
        teamResponse.setMessage(ServiceResponse.SUCCESS);
        return teamResponse;
    }

    public TeamResponse deleteTeam(Long teamId) {
        TeamResponse teamResponse = new TeamResponse();
        teamRepository.deleteById(teamId);
        teamResponse.setCode(HttpStatus.OK.value());
        teamResponse.setMessage(ServiceResponse.SUCCESS);
        return teamResponse;
    }
}
