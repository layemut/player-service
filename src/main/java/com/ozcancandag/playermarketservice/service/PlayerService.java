package com.ozcancandag.playermarketservice.service;

import com.ozcancandag.playermarketservice.entity.Player;
import com.ozcancandag.playermarketservice.entity.Team;
import com.ozcancandag.playermarketservice.model.PlayerResponse;
import com.ozcancandag.playermarketservice.model.ServiceResponse;
import com.ozcancandag.playermarketservice.model.dto.PlayerDto;
import com.ozcancandag.playermarketservice.repository.PlayerRepository;
import com.ozcancandag.playermarketservice.repository.TeamRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;


@Service
@Slf4j
@RequiredArgsConstructor
public class PlayerService {

    private final PlayerRepository playerRepository;
    private final TeamRepository teamRepository;

    public PlayerResponse queryAll(int page, int size) {
        PlayerResponse playerResponse = new PlayerResponse();
        Page<Player> playerPage = playerRepository.findAll(PageRequest.of(page, size));
        playerResponse.setCode(HttpStatus.OK.value());
        playerResponse.setMessage(ServiceResponse.SUCCESS);
        playerResponse.setPlayers(playerPage.getContent().stream().map(PlayerDto::new).collect(Collectors.toList()));
        return playerResponse;
    }

    public PlayerResponse queryPlayer(Long playerId) {
        PlayerResponse playerResponse = new PlayerResponse();
        playerRepository.findById(playerId).ifPresentOrElse(player -> {
            playerResponse.setPlayer(new PlayerDto(player));
            playerResponse.setCode(HttpStatus.OK.value());
            playerResponse.setMessage(ServiceResponse.SUCCESS);
        }, () -> {
            playerResponse.setCode(HttpStatus.NOT_FOUND.value());
            playerResponse.setMessage("Player not found.");
        });
        return playerResponse;
    }

    public PlayerResponse upsertPlayer(PlayerDto playerDto) {
        Player player = new Player(playerDto);
        Team team = teamRepository.findById(playerDto.getTeamId()).orElseThrow();
        player.setTeam(team);
        PlayerResponse playerResponse = new PlayerResponse();
        playerResponse.setPlayer(new PlayerDto(playerRepository.save(player)));
        playerResponse.setCode(HttpStatus.OK.value());
        playerResponse.setMessage(ServiceResponse.SUCCESS);
        return playerResponse;
    }

    public PlayerResponse deletePlayer(Long playerId) {
        PlayerResponse playerResponse = new PlayerResponse();
        playerRepository.deleteById(playerId);
        playerResponse.setCode(HttpStatus.OK.value());
        playerResponse.setMessage(ServiceResponse.SUCCESS);
        return playerResponse;
    }
}
