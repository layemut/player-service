package com.ozcancandag.playermarketservice.service;

import com.ozcancandag.playermarketservice.entity.Player;
import com.ozcancandag.playermarketservice.model.ServiceResponse;
import com.ozcancandag.playermarketservice.model.TransferResponse;
import com.ozcancandag.playermarketservice.repository.PlayerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransferService {

    private final PlayerRepository playerRepository;

    public TransferResponse chargeFee(Long playerId) {
        Player player = playerRepository.findById(playerId).orElseThrow();
        double contractFee = player.getTransferFee() + (player.getTransferFee() * player.getTeam().getCommissionRate() / 100);
        TransferResponse response = new TransferResponse(BigDecimal.valueOf(contractFee).setScale(2, RoundingMode.UP));
        response.setCode(HttpStatus.OK.value());
        response.setMessage(ServiceResponse.SUCCESS);
        return response;
    }
}
