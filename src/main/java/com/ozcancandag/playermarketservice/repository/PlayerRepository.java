package com.ozcancandag.playermarketservice.repository;

import com.ozcancandag.playermarketservice.entity.Player;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlayerRepository extends PagingAndSortingRepository<Player, Long> {
}
