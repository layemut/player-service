package com.ozcancandag.playermarketservice.repository;

import com.ozcancandag.playermarketservice.entity.Team;
import org.springframework.data.repository.CrudRepository;

public interface TeamRepository extends CrudRepository<Team, Long> {
}
