package com.ozcancandag.playermarketservice.web;


import com.ozcancandag.playermarketservice.model.TransferResponse;
import com.ozcancandag.playermarketservice.service.TransferService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/transfer")
@RequiredArgsConstructor
@Slf4j
public class TransferController {
    private final TransferService transferService;

    @GetMapping("/charge-fee/{playerId}")
    public ResponseEntity<TransferResponse> chargeTransferFee(@PathVariable("playerId") Long playerId) {
        TransferResponse response = transferService.chargeFee(playerId);
        return ResponseEntity.status(response.getCode()).body(response);
    }
}
