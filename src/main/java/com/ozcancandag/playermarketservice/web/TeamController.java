package com.ozcancandag.playermarketservice.web;

import com.ozcancandag.playermarketservice.model.TeamResponse;
import com.ozcancandag.playermarketservice.model.dto.TeamDto;
import com.ozcancandag.playermarketservice.service.TeamService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/team")
@RequiredArgsConstructor
@Slf4j
public class TeamController {

    private final TeamService teamService;

    @GetMapping("/{teamId}")
    public ResponseEntity<TeamResponse> queryTeam(@PathVariable("teamId") Long teamId) {
        TeamResponse response = teamService.queryTeam(teamId);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @PostMapping
    public ResponseEntity<TeamResponse> createPlayer(@RequestBody TeamDto team) {
        TeamResponse response = teamService.upsertTeam(team);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @DeleteMapping("/{teamId}")
    public ResponseEntity<TeamResponse> deletePlayer(@PathVariable("teamId") Long teamId) {
        TeamResponse response = teamService.deleteTeam(teamId);
        return ResponseEntity.status(response.getCode()).body(response);
    }
}
