package com.ozcancandag.playermarketservice.web;

import com.ozcancandag.playermarketservice.model.PlayerResponse;
import com.ozcancandag.playermarketservice.model.dto.PlayerDto;
import com.ozcancandag.playermarketservice.service.PlayerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/player")
@RequiredArgsConstructor
@Slf4j
public class PlayerController {

    private final PlayerService playerService;

    @GetMapping("/{playerId}")
    public ResponseEntity<PlayerResponse> queryPlayer(@PathVariable("playerId") Long playerId) {
        PlayerResponse response = playerService.queryPlayer(playerId);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @GetMapping(params = {"page", "size"})
    public ResponseEntity<PlayerResponse> queryAll(int page, int size) {
        return ResponseEntity.ok().body(playerService.queryAll(page, size));
    }

    @PostMapping
    public ResponseEntity<PlayerResponse> createPlayer(@RequestBody PlayerDto player) {
        PlayerResponse response = playerService.upsertPlayer(player);
        return ResponseEntity.status(response.getCode()).body(response);
    }

    @DeleteMapping("/{playerId}")
    public ResponseEntity<PlayerResponse> deletePlayer(@PathVariable("playerId") Long playerId) {
        PlayerResponse response = playerService.deletePlayer(playerId);
        return ResponseEntity.status(response.getCode()).body(response);
    }
}
